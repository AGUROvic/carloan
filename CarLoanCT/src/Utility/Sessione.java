package Utility;

public class Sessione {
	
	
	private static String tipologiaUtente; // strettamente collegato agli indici: la tipologia di utente pu� essere di 3 tipi : AMMINISTRATORE, DIRETTORE,IMPIEGATO
	private static String cf; // Serve per memorizzare il cf del direttore o del cliente.
	private static String targa; 
	private static String dataFine;
	private static int sedeUtente;
	private static int categoria;
	private static int sedeModifica;
	
	

	public static void login_D_I(String tipologia, int SedeAppartenenza){
		Sessione.setTipologiaUtente(tipologia);
		Sessione.setSedeUtente(SedeAppartenenza);
	}
	
	
	public static void login_A(String tipologia){
		Sessione.setTipologiaUtente(tipologia);
	}
	
	public static void logout(){
		Sessione.setTipologiaUtente(null);
		Sessione.setSedeUtente(0);
	}
	
	public static String getTipologiaUtente() {
		return tipologiaUtente;
	}



	public static void setTipologiaUtente(String tipologiaUtente) {
		Sessione.tipologiaUtente = tipologiaUtente;
	}



	public static String getCf() {
		return cf;
	}



	public static void setCf(String cf) {
		Sessione.cf = cf;
	}



	public static String getTarga() {
		return targa;
	}



	public static void setTarga(String targa) {
		Sessione.targa = targa;
	}



	public static String getDataFine() {
		return dataFine;
	}



	public static void setDataFine(String dataFine) {
		Sessione.dataFine = dataFine;
	}



	public static int getSedeUtente() {
		return sedeUtente;
	}



	public static void setSedeUtente(int sedeUtente) {
		Sessione.sedeUtente = sedeUtente;
	}



	public static int getCategoria() {
		return categoria;
	}



	public static void setCategoria(int categoria) {
		Sessione.categoria = categoria;
	}



	public static int getSedeModifica() {
		return sedeModifica;
	}



	public static void setSedeModifica(int sedeModifica) {
		Sessione.sedeModifica = sedeModifica;
	}

}
