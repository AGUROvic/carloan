package business_tier.TO;

public class LoginTO implements InterfaceTO {

	// VARIABILI DI ISTANZA

	private String username;
	private String password;
	private String cf;
	private int idSede;

	
	//GETTER AND SETTER
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCf() {
		return cf;
	}
	public void setCf(String cf) {
		this.cf = cf;
	}
	public int getIdSede() {
		return idSede;
	}
	public void setIdSede(int idSede) {
		this.idSede = idSede;
	}
	
}
