package business_tier.TO;
import java.sql.Date;

public class ClienteTO implements InterfaceTO{

	// VARIABILI DI ISTANZA

	private String cfCliente;
	private String nome;
	private String cognome;
	private Date dataNascita;
	private int tellCliente;
	private String emailCliente;

	//GETTER AND SETTER

	public String getCfCliente() {
		return cfCliente;
	}
	public void setCfCliente(String cfCliente) {
		this.cfCliente = cfCliente;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public Date getDataNascita() {
		return dataNascita;
	}
	public void setDataNascita(Date dataNascita) {
		this.dataNascita = dataNascita;
	}
	public int getTellCliente() {
		return tellCliente;
	}
	public void setTellCliente(int tellCliente) {
		this.tellCliente = tellCliente;
	}
	public String getEmailCliente() {
		return emailCliente;
	}
	public void setEmailCliente(String emailCliente) {
		this.emailCliente = emailCliente;
	}





}
