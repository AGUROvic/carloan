package business_tier.TO;
import java.sql.Date; // vedi su api
public class ContrattoTO implements InterfaceTO{


	// VARIABILI DI ISTANZA

	private int idContratto;
	private Date dataInizio;
	private Date dataFine;
	private Date dataConsegna;
	private float penale;
	private float acconto;
	private float saldo;
	private float importoTot;
	private float kmPercorsi;
	private int sedeInzio;
	private int sedeFine;
	private String targa;
	private String cliente;
	private String tipoContratto;
	private int fasciaKm;
	



	//GETTER AND SETTER


	public int getIdContratto() {
		return idContratto;
	}
	public void setIdContratto(int idContratto) {
		this.idContratto = idContratto;
	}
	public Date getDataInizio() {
		return dataInizio;
	}
	public void setDataInizio(Date dataInizio) {
		this.dataInizio = dataInizio;
	}
	public Date getDataFine() {
		return dataFine;
	}
	public void setDataFine(Date dataFine) {
		this.dataFine = dataFine;
	}
	public Date getDataConsegna() {
		return dataConsegna;
	}
	public void setDataConsegna(Date dataConsegna) {
		this.dataConsegna = dataConsegna;
	}
	public float getPenale() {
		return penale;
	}
	public void setPenale(float penale) {
		this.penale = penale;
	}
	public float getAcconto() {
		return acconto;
	}
	public void setAcconto(float acconto) {
		this.acconto = acconto;
	}
	public float getSaldo() {
		return saldo;
	}
	public void setSaldo(float saldo) {
		this.saldo = saldo;
	}
	public float getImportoTot() {
		return importoTot;
	}
	public void setImportoTot(float importoTot) {
		this.importoTot = importoTot;
	}
	public float getKmPercorsi() {
		return kmPercorsi;
	}
	public void setKmPercorsi(float kmPercorsi) {
		this.kmPercorsi = kmPercorsi;
	}
	public int getSedeInzio() {
		return sedeInzio;
	}
	public void setSedeInzio(int sedeInzio) {
		this.sedeInzio = sedeInzio;
	}
	public int getSedeFine() {
		return sedeFine;
	}
	public void setSedeFine(int sedeFine) {
		this.sedeFine = sedeFine;
	}
	public String getTarga() {
		return targa;
	}
	public void setTarga(String targa) {
		this.targa = targa;
	}
	public String getCliente() {
		return cliente;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public String getTipoContratto() {
		return tipoContratto;
	}
	public void setTipoContratto(String tipoContratto) {
		this.tipoContratto = tipoContratto;
	}
	public int getFasciaKm() {
		return fasciaKm;
	}
	public void setFasciaKm(int fasciaKm) {
		this.fasciaKm = fasciaKm;
	}










}
