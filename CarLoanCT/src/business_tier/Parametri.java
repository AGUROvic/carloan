package business_tier;
import java.util.ArrayList;


public class Parametri<T> {
	
	ArrayList<T> parametri = new ArrayList<T>();
	
	/**
	 * Questo metodo permette l'aggiunta di un qualsiasi tipo di oggetto all' arraylist.
	 * @param element � l'elemento che si vuol aggiungere all'arraylist.
	 */
	public void add(T element){
		parametri.add(element);
	}
	
	/**
	 * Questo metodo restituisce l'oggetto alla posizione index dell'arraylist.
	 * @param index � l'indice dell'elemento che si vuol restituire.
	 * @return l'oggetto alla posizione index.
	 */
	public T get(int index){
		return parametri.get(index);
	}
	
	/**
	 * Permete la restituzione dell'intero arraylist.
	 * @return l'arraylist.
	 */
	public ArrayList<T> getAll(){
		return parametri;
	}
	
	/**
	 * Questo metodo restituisce la lunghezza dell'arraylist.
	 * @return la lunghezza dell'arraylist.
	 */
	public int size(){
		return parametri.size();
	}
	
	/**
	 * Questo metodo controlla se l'array � vuoto oppure no.
	 * @return true se � vuoto, false altrimenti.
	 */
	public boolean isEmpty(){
		return parametri.isEmpty();
	}
	
	/**
	 * Questo metodo elimina gli elementi presenti nell'arraylist.
	 */
	public void clear(){
		parametri.clear();
	}

}
