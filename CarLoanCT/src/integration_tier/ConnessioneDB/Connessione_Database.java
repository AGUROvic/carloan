package integration_tier;
import com.mysql.jdbc.Driver; 
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

public class Connessione_Database {

	protected static Connection conn = null; // connessione al database
	protected Statement stat;
	protected PreparedStatement prepSt;
	protected ResultSet res;
	

	//nome del driver JDBC e URL del database

	static final String JDBC = "com.mysql.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost/carloan";

	public static void connessione(){

		String username = "root";
		String password = "password";
		conn = null;
		
		try {
			Class.forName(JDBC); // pu� sollevare ClassNotFoundEcxeption. Returns the Class object associated with the class or interface with the given string name.
			conn = DriverManager.getConnection( DB_URL, username,password); //stabilisce la connessione
			JOptionPane.showMessageDialog(null, "Connessione stabilita correttamente!.");
		}
		catch(ClassNotFoundException e){
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Attenzione il driver del database non � stato trovato.");
		}
		catch(SQLException e){
			e.printStackTrace();
			
			if(e.getErrorCode() == 1049){
				System.out.println("Attenzione! Database non presente.");
				JOptionPane.showMessageDialog(null, "Attenzione! Il database rischiesto non � presente.");
			}
			else{
				if(e.getErrorCode() == 1045){
					
					JOptionPane.showMessageDialog(null, "Attenzione username o password errati.");
				}
				else {
					JOptionPane.showMessageDialog(null,"Errore nella connessione al database! \n Contattare l'amministratore di sistema.\nIl sistema verr� chiuso ora.");
				}
			}
		}
	}


	// Pattern singleton: permette di mantenere un'unica istanza della connessione aperta.. cos� facendo
	// non occorre aprire ogni volta una nuova connessione al DB per ogni singola azione su di esso..
	
	public static Connection getInstance(){
		if (conn == null) {
			connessione();
		}
		return conn;
	}
	
	
	
	
}
