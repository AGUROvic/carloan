package integration_tier.DAO;

import java.sql.SQLException;

import javax.swing.JOptionPane;

import business_tier.Parametri;
import business_tier.TO.InterfaceTO;
import business_tier.TO.VeicoloTO;
import integration_tier.Connessione_Database;

public class VeicoloDAO extends Connessione_Database implements InterfacciaDAO {

	@Override
    public void create(Parametri<InterfaceTO> parametro) {
       
        String query = "";
        VeicoloTO ved = (VeicoloTO) parametro.get(0);
       
        String targa = ved.getTarga();
        String modello = ved.getModello();
        int km = ved.getKm();
        String stato = ved.getStato();
        int sede = ved.getSede();
        int categoria = ved.getCategoria();
        String alimentazione = ved.getAlimentazione();
        int porte = ved.getPorte();
        int posti = ved.getnPosti();
        
       
        query = "INSERT INTO veicolo(targa,modello,Chilometri,SedeV,Stato,Categoria,Alimentazione,Porte,nPosti) values('"
                + targa + "','" + modello + "','" + km + "','" + sede + "','" + stato + "','" + categoria + "','" + alimentazione + "','" + porte + "','" + posti
                + "');";
       
        try{
            getInstance();
            stat = conn.createStatement();
            stat.executeUpdate(query);
            JOptionPane.showMessageDialog(null, "Veicolo creato con successo");
            parametro.clear();
        }
       
        catch(SQLException e){
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Query per la creazione di un veicolo fallita.");
            parametro.clear();
        }
    }

	@Override
	public void delete(Parametri<InterfaceTO> parametro) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(Parametri<InterfaceTO> paramtro) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void search(Parametri<InterfaceTO> parametri) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void list(Parametri<InterfaceTO> parametri) {
		// TODO Auto-generated method stub
		
	}

}
