package integration_tier.DAO;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import business_tier.Parametri;
import business_tier.TO.CategoriaTO;
import business_tier.TO.InterfaceTO;
import business_tier.TO.SedeTO;
import integration_tier.Connessione_Database;

public class SedeDAO extends Connessione_Database implements InterfacciaDAO  {

	@Override
	public void create(Parametri<InterfaceTO> parametro) {

		String query ="";
		SedeTO sdt = (SedeTO) parametro.get(0);
		String nome = sdt.getNome();
		String indirizzo = sdt.getIndirizzo();
		String citt� = sdt.getCitt�();
		String telefono = sdt.getTel();
		int idSede = sdt.getIdSede();

		ArrayList<String> tells = new ArrayList<String>();
		tells = getTells();

		if(isPresent(telefono, tells) == true){
			JOptionPane.showMessageDialog(null, "Attenzione esist� gi� una sede con associato il seguente numero telefonico: " + telefono);
		}
		else{
			
			ArrayList<Integer> ida = new ArrayList<Integer>();
			ida = getIdSede();
			
			if(isPresent(idSede, ida) == true){
				JOptionPane.showMessageDialog(null, "Attenzione esiste gi� una sede con questo id, assegneremo alla vostra sede il prossimo valore disponibile");
			}
			
			query = "INSERT INTO sede(Nome,via,Telefono,Citta) values ('"
					+ nome + "','" + indirizzo + "','" + telefono + "','" + citt�
					+ "');";

			try{
				stat = conn.createStatement();
				stat.executeUpdate(query);
				JOptionPane.showMessageDialog(null, "Sede creata con successo");
				parametro.clear();
			}
			catch(SQLException e){
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, "Query fallita.");
				parametro.clear();
			}
		}
	}

	@Override
	public void delete(Parametri<InterfaceTO> parametro) {

		SedeTO sto = (SedeTO) parametro.get(0);
		int sede = sto.getIdSede();	

		ArrayList<Integer> cat = getIdSede();
		if(cat.size() == 0){
			JOptionPane.showMessageDialog(null, "Attenzione, non sono presenti sedi nel DB.");
		}
		else{	

			if(isPresent(sede, cat) == false){
				JOptionPane.showMessageDialog(null, "Attenzione, la sede che si vuole eliminare dal db non esiste.");
			}
			else{

				String query = "DELETE FROM sede WHERE id_sede='" + sede + "';";

				try{

					stat = conn.createStatement();
					stat.executeUpdate(query);
					JOptionPane.showMessageDialog(null, "La sede '" + sede + "' � stata eliminata dal DB");
					parametro.clear();
				}
				catch(SQLException e){
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, "Problemi durante l'eliminazione.\n la sede : \n '" + sede + "' non � presente nel DB");
					parametro.clear();
				}
			}
		}

	}



	@Override
	public void update(Parametri<InterfaceTO> parametro) {

		String query ="";
		SedeTO sdt = (SedeTO) parametro.get(0);

		String nome = sdt.getNome();
		int idSede = sdt.getIdSede();
		String indirizzo = sdt.getIndirizzo();
		String citt� = sdt.getCitt�();
		String telefono = sdt.getTel();

		query = " UPDATE sede SET nome = '"
				+ nome + "', citta = '" + citt� + "', via = '" + indirizzo
				+ "', telefono = '" + telefono + "' where id_Sede = " + idSede + ";";

		try{
			getInstance();
			stat = conn.createStatement();
			stat.executeUpdate(query);
			JOptionPane.showMessageDialog(null, "Sede modificata con successo");
			parametro.clear();

		}
		catch(SQLException e){
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Query fallita");
			parametro.clear();
		}



	}


	@Override
	public void search(Parametri<InterfaceTO> parametro) {

		SedeTO sto = (SedeTO) parametro.get(0);
		int idSede = sto.getIdSede();	

		ArrayList<Integer> sede = getIdSede();
		if(isPresent(idSede, sede) == false){
			JOptionPane.showMessageDialog(null, "L'id della sede immesso non esiste all'interno del database.");
		}
		else{

			String query = "SELECT * FROM carloan.sede WHERE id_sede = '" + idSede + "';";
			try{
				stat = conn.createStatement();
				res = stat.executeQuery(query);

				while(res.next()){
					SedeTO sto_res = new SedeTO();
					sto_res.setCitt�(res.getString("citta"));
					sto_res.setIdSede(res.getInt("id_sede"));
					sto_res.setIndirizzo(res.getString("via"));
					sto_res.setNome(res.getString("nome"));
					sto_res.setTel(res.getString("telefono"));				
					parametro.add(sto_res);
				}
			}catch(SQLException e ){
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, "Query per la ricerca della sede con il seguente id : " + idSede  + ", fallita.");
				parametro.clear();
			}
		}
	}

	@Override
	public void list(Parametri<InterfaceTO> parametri) {

		String query ="";

		query = "SELECT * FROM sede";

		try{
			connessione();

			stat = conn.createStatement();
			res = stat.executeQuery(query);

			while(res.next()){
				SedeTO sdt = new SedeTO();

				sdt.setIdSede(res.getInt("id_sede"));
				sdt.setNome(res.getString("nome"));
				sdt.setCitt�(res.getString("citta"));
				sdt.setIndirizzo(res.getString("via"));
				sdt.setTel(res.getString("telefono"));

				parametri.add(sdt);
			}
			JOptionPane.showMessageDialog(null, "Lista delle sedi presenti nel DB restituita con successo.");
		}
		catch(SQLException e){
			e.printStackTrace();
			parametri = null;
			JOptionPane.showMessageDialog(null, "Query non riuscita.");
		}

	}

	public void listEmptySeat(Parametri<InterfaceTO> parametri){

		String query = "select Id_sede,nome from sede where Id_sede not in(select id_sede from direttore);";

		try{
			connessione();
			stat = conn.createStatement();
			res = stat.executeQuery(query);

			while(res.next()){
				SedeTO sdt = new SedeTO();
				sdt.setIdSede(res.getInt("id_sede"));
				sdt.setNome(res.getString("nome"));
				parametri.add(sdt);
			}
		}
		catch(SQLException e){
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "query fallita");	
		}




	}

	private ArrayList<String> getTells(){

		ArrayList<String>  tells = new ArrayList<String>();
		String query = "SELECT telefono from carloan.sede;";
		try{
			getInstance();
			stat = conn.createStatement();
			res = stat.executeQuery(query);
			while(res.next()){
				tells.add(res.getString("telefono"));
			}
		}catch(SQLException e){
			e.printStackTrace();

		}
		return tells;
	}


	private ArrayList<Integer> getIdSede(){

		ArrayList<Integer> cat = new ArrayList<Integer>();

		String query = "select id_sede from carloan.sede;";

		try {
			getInstance();
			stat = conn.createStatement();
			res = stat.executeQuery(query);
			while(res.next()){
				cat.add(res.getInt("id_sede"));

			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return cat;
	}



	/**
	 * Controlla se nell'arrayList � presente l'id sede (intero) passato come parametro formale.
	 * @param idSede
	 * @param sedi
	 * @return true se la stringa cf � contenuta nell'arrayList, false altrimenti.
	 */
	private boolean isPresent(int idSede, ArrayList<Integer> sedi){

		int i = 0;
		boolean present = false;

		while(i < sedi.size() && !present){

			if((sedi.get(i) == idSede)){
				present = true;

			}
			i++;
		}
		return present;

	}

	private boolean isPresent(String tell, ArrayList<String> tells){

		int i = 0;
		boolean present = false;

		while(i < tells.size() && !present){

			if((tells.get(i).compareToIgnoreCase(tell) == 0)){
				present = true;
			}
			i++;
		}
		return present;

	}
}



