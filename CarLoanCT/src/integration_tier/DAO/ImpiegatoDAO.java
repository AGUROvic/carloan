package integration_tier.DAO;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import Utility.Sessione;
import business_tier.Parametri;
import business_tier.TO.ImpiegatoTO;
import business_tier.TO.InterfaceTO;
import business_tier.TO.LoginTO;
import integration_tier.Connessione_Database;

public  class ImpiegatoDAO extends Connessione_Database implements InterfacciaDAO  {

	/**
	 * Inserisce un oggetto di tipo ImpiegatoTO nel db. Controlla che non sia gi� presente un impiegato con lo stesso cf
	 * all'interno del db prima di procedere con la query.
	 */
	@Override
	public void create(Parametri<InterfaceTO> parametro) {
		ImpiegatoTO ito = (ImpiegatoTO) parametro.get(0);

		String query = "";
		String cf = ito.getCf();
		String nome = ito.getNome();
		String cognome = ito.getCognome();
		int idSede = ito.getSede();
		String mail = ito.getMail();
		String psw = ito.getPassword();

		ArrayList<String> cfs = new ArrayList<String>();
		cfs = getCfs();


		if(isPresent(cf, cfs) == true){
			JOptionPane.showMessageDialog(null, "Impossibile inserire il seguente impiegato, � gi� presente un impiegato con il seguente cf: \n " + cf);
		}
		else{
			query = "INSERT INTO impiegato(cf,nome,cognome,password,mail,id_sede) values ('"
					+ cf + "','"
					+ nome + "','"
					+ cognome + "','"
					+ psw + "','"
					+ mail + "'," 
					+ idSede + ");";
			try{
				stat = conn.createStatement();
				stat.executeUpdate(query);
				JOptionPane.showMessageDialog(null, "Impiegato inserito correttamente.");
				parametro.clear();
			}
			catch(SQLException e){
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, "Problemi nell'inserimento dell'impiegato. Query fallita.");
				parametro.clear();
			}
		}

	}

	/**
	 * Elimina un impiegato dal db mediante il suo cf.
	 *  Nel caso in cui la tabella 'impiegato' � vuota verr� mostrato un messaggio che ci informa della situazione.
	 *   Nel caso in cui vogliamo eliminare un impiegato che non � presente nel db verremo informati.
	 *   
	 */
	@Override
	public void delete(Parametri<InterfaceTO> parametro) {

		ImpiegatoTO ito = (ImpiegatoTO) parametro.get(0);
		String cf = ito.getCf();	

		ArrayList<String> cfs = getCfs();
		if(cfs.size() == 0){
			JOptionPane.showMessageDialog(null, "Attenzione, non sono presenti impiegati nel DB.");
		}
		else{	

			if(isPresent(cf, cfs) == false){
				JOptionPane.showMessageDialog(null, "Attenzione, l'impiegato che si vuole eliminare dal db non esiste.");
			}
			else{

				String query = "DELETE FROM impiegato WHERE cf='" + cf + "';";

				try{

					stat = conn.createStatement();
					stat.executeUpdate(query);
					JOptionPane.showMessageDialog(null, "L'Impiegato con il CF: '" + cf + "' � stato eliminato dal DB");
					parametro.clear();
				}
				catch(SQLException e){
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, "Problemi durante l'eliminazione.\n l'impiegato con il seguente CF: \n '" + cf + "' non � presente nel DB");
					parametro.clear();
				}
			}
		}
	}

	
	@Override
	public void update(Parametri<InterfaceTO> parametro) {

//		String query = "";
//		ImpiegatoTO cat = (ImpiegatoTO) parametro.get(0);
//		String cf = cat.getCf();
//		String nome = cat.getNome();
//		String cognome = cat.getCognome();
//		String mail = cat.getMail();
//
//		query = "UPDATE impiegato SET cf='" + cf + "',nome='" + nome
//				+ "',cognome='" + cognome + "',mail='" + mail
//				+ "' where cf='" + Sessione.getCf() + "';";
//
//		try {
//			getInstance();
//			stat = conn.createStatement();
//			stat.execute(query);
//			JOptionPane.showMessageDialog(null, "Modifica effettuata correttamente.");
//			parametro.clear();
//
//		} catch (Exception e) {
//			e.printStackTrace();
//			JOptionPane.showMessageDialog(null, "Problemi durante la modifica. Query fallita.");
//			parametro.clear();
//		}
//
}

	@Override
	public void search(Parametri<InterfaceTO> parametro) {

		ImpiegatoTO ito = (ImpiegatoTO) parametro.get(0);
		String cf = ito.getCf();
		
		ArrayList<String> cfs = getCfs();
		if(isPresent(cf, cfs) == false){
			JOptionPane.showMessageDialog(null, "Il codice fiscale immesso non � associato ad alcun impiegato.");
		}
		else{
			
			String query = "SELECT * FROM carloan.impiegato WHERE cf = '" + cf + "';";
			try{
				stat = conn.createStatement();
				res = stat.executeQuery(query);
				
				while(res.next()){
					ImpiegatoTO ito_res = new ImpiegatoTO();
					ito_res.setCf(res.getString("cf"));
					ito_res.setCognome(res.getString("cognome"));
					ito_res.setNome(res.getString("nome"));
					ito_res.setMail(res.getString("mail"));
					ito_res.setPassword(res.getString("password"));
					parametro.add(ito_res);
				}
			}catch(SQLException e ){
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, "Query per la ricerca dell' impiegato con il seguente cf : " + cf + ", fallita.");
				parametro.clear();
			}
		}
	}

	@Override
	public void list(Parametri<InterfaceTO> parametro) {


		ArrayList<String> cfs = getCfs();
		if(cfs.size() == 0){
			JOptionPane.showMessageDialog(null, "Attenzione, non sono presenti impiegati nel DB.");
		}
		else{

			String query = "select * from impiegato";

			try{

				stat = conn.createStatement();
				res = stat.executeQuery(query);

				while (res.next()) {
					ImpiegatoTO cat = new ImpiegatoTO();
					cat.setCf(res.getString("cf"));
					cat.setNome(res.getString("nome"));
					cat.setCognome(res.getString("cognome"));
					cat.setPassword(res.getString("password"));
					cat.setSede(res.getInt("id_sede"));
					cat.setMail(res.getString("mail"));
					parametro.add(cat);
				}
				JOptionPane.showMessageDialog(null, "Query effettuata con successo");

			} catch (SQLException e) {
				parametro = null;
				JOptionPane.showMessageDialog(null, "QueryErrata.");
			}
		}
	}

	/**
	 * Genera un array di tipo String che contiene i cf degli impiegati.
	 * @return un array di tipo String contenente i cf degli impiegati.
	 */
	private ArrayList<String> getCfs(){

		ArrayList<String> cfs = new ArrayList<String>();

		String query = "select cf from carloan.impiegato;";

		try {
			getInstance();
			stat = conn.createStatement();
			res = stat.executeQuery(query);
			while(res.next()){
				cfs.add(res.getString("cf"));
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return cfs;
	}



	/**
	 * Controlla se nell'arrayList � presente la stringa cf passata come parametro formale.
	 * @param cf 
	 * @param cfs
	 * @return true se la stringa cf � contenuta nell'arrayList, false altrimenti.
	 */
	private boolean isPresent(String cf, ArrayList<String> cfs){

		int i = 0;
		boolean present = false;

		while(i < cfs.size() && !present){

			if((cfs.get(i).compareToIgnoreCase(cf) == 0)){
				present = true;
			}
			i++;
		}
		return present;

	}






}
